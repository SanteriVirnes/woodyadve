﻿using UnityEngine;
using System.Collections;

public class Item : MonoBehaviour {

    public string description = "It just a thing.";
    public bool isPickable = false;
	public float type=2.0f;

    virtual public void OnClick( out string resultDialog )
    {
        resultDialog = null;
    }


    virtual public bool OnUse(Item item, out string resultDialog)
    {
        resultDialog = null;
        return false;
    }
}
