﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour {
	public GameObject player;
	public Transform spawn;

	// Use this for initialization
	void Start () {
		if (GameObject.Find ("Peasant(Clone)") == null) {
			Instantiate (player, spawn.position, Quaternion.AngleAxis (90, Vector3.up));
		} else {
			GameObject.Find("Peasant(Clone)").transform.position = spawn.position;
		}

	
	}
	
	// Update is called once per frame
	void Update () {
		this.transform.position = new Vector3 (GameObject.Find ("Peasant(Clone)").transform.position.x, this.transform.position.y, this.transform.position.z);
	}
}
